# 2.3.git_introduction - Модуль №2 Занятие №3

## Задание

Вам необходимо познакомиться с git.
Для этого выполните следующее:

### 1. Скопируйте ссылку репозитория
https://gitlab.com/devhouse_frontend/second_module/2.3.git_introduction.git
### 2. Откройте с помощью IDE папку, где будет размещена локальная версия проекта
### 3. Откройте терминал и выполните
```
git clone https://gitlab.com/devhouse_frontend/second_module/2.3.git_introduction.git
```
### 4. В терминале переместитесь в директорию проекта
```
cd 2.3.git_introduction.git
```
### 5. Переключитесь на ветку для разработки
```
git checkout development
```
Необходимо убедиться, что в ветке вы видите html-страницу задания.
Чтобы проверить состояние вашего репозитория выполните:
```
git status
```
### 5. Создайте свою ветку
```
git checkout -b 'название_вашей_ветки'
```
### 6. В список имен учеников на странице добавьте свое имя
При клике на имя должно появляться базовое интерактивное окно `alert` с приветствием и вашим именем.
Сохраните измененный файл.
### 7. Необходимо сохранить ваши изменения в git локально
```
git add /название_измененного_файла
git commit -m 'ваш комментарий'
```
### 8. Разместите изменения на удаленном репозитории
Если на удаленном репозитории еще нет вашей ветки:
```
git push --set-upstream origin название_вашей_ветки
```
Если ветка уже существует - достаточно выполнить:
```
git push
```


## Полезные материалы
Изучение git:
1. Документация <a href='https://git-scm.com/doc'> Git - Documentation </a>
2. Удобная обучалка <a href='https://learngitbranching.js.org/'> Learn Git Branching </a>

Необходимое программное обеспечение можно найти:
1. IDE <a href='https://www.jetbrains.com/webstorm/'> WebStorm </a> либо <a href='https://code.visualstudio.com/'> Visual Studio Code </a> 
2. Важно не забыть - <a href='https://nodejs.org/ru/'> Node.js </a> 
3. Git это тоже программа - <a href='https://git-scm.com/downloads'> download | GIT </a>

